const comOptions = ['batu', 'gunting', 'kertas'];
let isPlayed = false;

class Game {
     constructor() {
          if (this.constructor === Game) {
               throw new Error('Abstract class');
          }

          this.pick = "";
     }

     addBg(player, pick) {
          let batuPick, guntingPick, kertasPick;

          if (player === "player") {
               batuPick= document.getElementById("player-batu");
               guntingPick= document.getElementById("player-gunting");
               kertasPick= document.getElementById("player-kertas");
          } else if (player === "com") {
               batuPick= document.getElementById("com-batu");
               guntingPick= document.getElementById("com-gunting");
               kertasPick= document.getElementById("com-kertas");   
          }

          if (pick == "batu") {
               batuPick.classList.add('picked');
          } else if (pick == "gunting") {
               guntingPick.classList.add('picked');
          } else if (pick == "kertas") {
               kertasPick.classList.add('picked');
          }
     }

     checkWin(comPick, playerPick) {
          if (comPick === playerPick) {
               return "draw";
          } else if (playerPick === "batu") {
               if (comPick === "gunting") {
                    return "win";
               } else if (comPick === "kertas") {
                    return "lose";
               }
          } else if (playerPick === "gunting") {
               if (comPick === "kertas") {
                    return "win";
               } else if (comPick === "batu") {
                    return "lose";
               }
          } else if (playerPick === "kertas") {
               if (comPick === "batu") {
                    return "win";
               } else if (comPick === "gunting") {
                    return "lose";
               }
          }
     }

     showResult(result) {
          document.getElementById("vs").classList.add("visually-hidden");
          if (result === "win") {
               document.getElementById("player-win").classList.remove("visually-hidden");
          } else if (result === "lose") {
               document.getElementById("com-win").classList.remove("visually-hidden");
          } else {
               document.getElementById("draw").classList.remove("visually-hidden");
          }

          isPlayed = true;
     }

     refresh() {
          isPlayed = false;
          document.getElementById("vs").classList.remove("visually-hidden");
          document.getElementById("player-win").classList.add("visually-hidden");
          document.getElementById("com-win").classList.add("visually-hidden");

          document.querySelectorAll(".picked").forEach(e => e.classList.remove('picked'));

          console.log("refresh");
     }

     play(playerPick, comPick) {
          if (isPlayed) {
               return;
          }
          this.addBg("player", playerPick);
          this.addBg("com", comPick);
          let result = this.checkWin(comPick, playerPick);
          this.showResult(result);
     }
}

class GamePlayer extends Game {
     constructor() {
          super();
     }
}


class GameCom extends Game {
     constructor() {
          super();
     }

     randomize(limit = 3) {
          let rand = Math.random() * limit;
          return Math.floor(rand);
     }

     #getComPick() {
          let rand = this.randomize(3);
          return comOptions[rand];
     }

     generateComPick() {
          const comPick = this.#getComPick();
          this.addBg("com", comPick);
          return comPick;
     }
}

const playerPick = (playerPick) => {
     let comPick;
     if (isPlayed === false) {
          new GamePlayer().addBg("player", playerPick);
          comPick = new GameCom().generateComPick();
          new GamePlayer().play(playerPick, comPick);
     }
}

const restart = () => {
     new GamePlayer().refresh();
}